/*
 *  FirstProgram.cpp
 *  
 *
 *  Created by Olivier Strauss on 10/10/16.
 *  Copyright 2016 LIRMM. All rights reserved.
 *
 */

#include "CImg.h"
#include <string.h>
#include <random>
#include <cstdlib>
#include <ctime>
#include <iostream>

using namespace cimg_library;

#define PRINT_MATRIX(matrix, numRows, numCols) \
    do { \
        auto it = matrix.begin(); \
        for (int lin = 0; lin < numRows; lin++) { \
            for (int col = 0; col < numCols; col++, it++) { \
                printf("[%g]", (*it)); \
            } \
            printf("\n"); \
        } \
    } while (0)

// Calcul de la multiplication C = A.B au sens des matrices.
// C est NlinxNcol, A est NlinxNcom, B est NcomxNcol
// Le pointeurs doivent etre alloues correctement avant l'appel
int MatMult(double *A, double *B, double *C, int Nlin, int Ncom, int Ncol)
{
	int lin, col, k ;
	double *ptA, *ptB, *ptC ;
	
	ptC = C ;
	for(lin = 0 ; lin<Nlin ; lin++)
	{
		for(col = 0 ; col<Ncol ; col++, ptC++)
		{
			ptA = A + (lin*Ncom) ;
			ptB = B + col ;
			(*ptC) = 0.0 ;
			for(k = 0 ; k<Ncom ; k++)
			{
				(*ptC) += (*ptA) * (*ptB) ;
				ptA++ ; 
				ptB+= Ncol ; 
			}
		}
	}
	return 1 ;
}

// Donne des 2 points d'intersection entre la droite
// de parametre L et le carre de dimension Dx, Dy
int Intersection( double L[3], int Dx, int Dy, int x_inter[2], int y_inter[2] )
{
	double a=L[0], b=L[1], c=L[2] ;
	double x[4], y[4] ;
	int nb_points_ok = 0, n ;
	x[0] = 0 ;
	x[1] = Dx-1 ;
	y[2] = 0 ;
	y[3] = Dy-1 ;
	
	if(fabs(L[0])>1e-16)
	{ // droite de la forme x = b'y + c' ;
		b = -L[1]/L[0] ;
		c = -L[2]/L[0] ;
		x[2] = b * y[2] + c ;
		x[3] = b * y[3] + c ;
	}
	else 
	{
		x[2] = -Dx ;
		x[3] = -Dx ;
	}

	if(fabs(L[1])>1e-16)
	{ // droite de la forme x = b'y + c' ;
		a = -L[0]/L[1] ;
		c = -L[2]/L[1] ;
		y[0] = a * x[0] + c ;
		y[1] = a * x[1] + c ;
	}
	else 
	{
		y[0] = -Dy ;
		y[1] = -Dy ;
	}
	
	for(n=0 ; n<4 ; n++)
	{
		if( (x[n]>=0.0) && (y[n]>=0.0) && (x[n]<=Dx) && (y[n]<=Dy) && (nb_points_ok<2) )
		{
			x_inter[nb_points_ok] = (int)floor(x[n]+0.5) ;
			y_inter[nb_points_ok] = (int)floor(y[n]+0.5) ;
			nb_points_ok ++ ;
		}
	}
	
	if(nb_points_ok==2) return 1 ; 
	else return 0 ;	
}


int main(int argc, char *argv[])
{
	int nombre_de_points = 12, n=0 ;
	int xd[nombre_de_points], yd[nombre_de_points], xg[nombre_de_points], yg[nombre_de_points] ;
	char droite_gaughe = 'd' ;
	
	if(argc<2) 
	{
		printf("\nCe programme a deux arguments d'appel qui sont les deux images droite et gauche\n") ;
		printf("Il faut le lancer de la forme ./Stereo ImageDroite.tif ImageGauche.tif\n") ;
		return 0 ;
	}
	
	// Chargement des deux images dont les noms ont Ã©tÃ© passÃ©s au programme principal
	CImg<unsigned char> imageD(argv[1]),imageG(argv[2]) ;

	// Definition des couleurs de trace 
	const unsigned char red[] = { 255,0,0 }, green[] = { 0,255,0 }, blue[] = { 0,0,255 }; // (rouge, vert, bleu)
    const unsigned char rose[] = { 255, 105, 180 };  // Rose
    const unsigned char violet[] = { 138, 43, 226 }; // Violet
    const unsigned char argent[] = { 192, 192, 192 }; // Argent
    const unsigned char gold[] = { 255, 215, 0 }; // Or
    const unsigned char camoisie[] = { 218, 112, 214 }; // Camoisie
    const unsigned char mer[] = { 0, 102, 204 }; // Mer
    const unsigned char star[] = { 255, 255, 153 }; // Star 


	// Creation des objets d'affichage
	CImgDisplay Droite_disp(imageD,"Image droite"), Gauche_disp(imageG,"Image gauche");
	
	// Selection de nombre_de_points paires de points (en commencant par l'image droite
	while (!Droite_disp.is_closed() && !Gauche_disp.is_closed() && n<nombre_de_points) 
	{
		switch (droite_gaughe) 
		{
			case 'd' :
  		Gauche_disp.set_title("%s","Image gauche");
		  Droite_disp.set_title("%s","Cliquez ici") ;
				Droite_disp.wait();
				if (Droite_disp.button() && Droite_disp.mouse_y()>=0) 
				{
					yd[n] = Droite_disp.mouse_y();
					xd[n] = Droite_disp.mouse_x();
					imageD.draw_circle(xd[n],yd[n],3,red,1.0,1).display(Droite_disp);
					droite_gaughe = 'g' ;
				} break ;
			case 'g' :
		  Droite_disp.set_title("%s","Image droite") ;
				Gauche_disp.set_title("%s","Cliquez ici");
				Gauche_disp.wait();
				if (Gauche_disp.button() && Gauche_disp.mouse_y()>=0) 
				{
					yg[n] = Gauche_disp.mouse_y();
					xg[n] = Gauche_disp.mouse_x();
					imageG.draw_circle(xg[n],yg[n],3,blue,1.0,1).display(Gauche_disp);
					droite_gaughe = 'd' ; n++ ;
				} break ;
			default : break;
		}		
	}
	
	// Affichage de tous les points en vert
	for(n=0 ; n<nombre_de_points ; n++) 
	{
		imageD.draw_circle(xd[n],yd[n],3,green,1.0,1).display(Droite_disp);
		imageG.draw_circle(xg[n],yg[n],3,green,1.0,1).display(Gauche_disp);
	}
	
	// Selection de deux points dans l'image droite et affichage de la droite passant par ces deux points
	n=0 ;
	double delta, L[3] ;
	int x_inter[2], y_inter[2] ;
	while (!Droite_disp.is_closed() && !Gauche_disp.is_closed() && n<2) 
	{
		Droite_disp.set_title("%s","Cliquez deux points") ;
		Droite_disp.wait();
		if (Droite_disp.button() && Droite_disp.mouse_y()>=0) 
		{
			yd[n] = Droite_disp.mouse_y();
			xd[n] = Droite_disp.mouse_x();
			imageD.draw_circle(xd[n],yd[n],3,red,1.0,1).display(Droite_disp);
			n = n+1 ;
		}			
	}
	delta = (double)xd[0]*(double)yd[1] - (double)xd[1]*(double)yd[0] ;
	imageD.draw_line(xd[0],yd[0],xd[1],yd[1],green).display(Droite_disp);

	L[0] = (double)( yd[1] - yd[0] ) ;
	L[1] = (double)( xd[0] - xd[1] ) ;
	L[2] = (double)xd[1]*(double)yd[0] - (double)xd[0]*(double)yd[1] ;		
			
	n = Intersection(L, imageD.width(), imageD.height(), x_inter, y_inter ) ;
	if(n)	imageD.draw_line(x_inter[0],y_inter[0],x_inter[1],y_inter[1],red).display(Droite_disp);
	
 // Partie calcul sur les matrices
 
	// Definition d'une matrice de 5 lignes et 3 colonnes (et un plan) en double precision
	// remplie au depart de 0
	
	CImg <double> matrice_A(5,3,1,1,0) ; 
	CImg <double>::iterator it ; // defiition d'un iterateur (permet d'avoir le premier element de la matrice)
	int lin, col, NlinA, NcolA, NlinB, NcolB, NlinC, NcolC ;
	
	NlinA = matrice_A.height() ;
	NcolA = matrice_A.width() ;
	
	// Remplissage de la matrice A avec des eux paires de points, tracer une droite de para-
	matrice_A.rand(0,10) ;
	
	// Affichage de la matrice A
	it = matrice_A.begin() ;
	for( lin=0 ; lin<NlinA ; lin++)
	{
		for( col=0 ; col<NcolA ; col++, it++)
		{
			printf("[%g]",(*it)) ;
		}
		printf("\n") ;
	}
	
 // Definition de la matrice B comme etant la pseudo-inverse de A 
	CImg <double> matrice_B =	matrice_A.pseudoinvert() ;
	printf("\n\n\n") ;
	
	NlinB = matrice_B.height() ;
	NcolB = matrice_B.width() ;
	
	// Affichage de la matrice B
	it = matrice_B.begin() ;
	for( lin=0 ; lin<NlinB ; lin++)
	{
		for( col=0 ; col<NcolB ; col++, it++)
		{
			printf("[%g]",(*it)) ;
		}
		printf("\n") ;
	}
	
	CImg <double> matrice_C(NlinA,NcolB,1,1,0) ;
	NlinC = matrice_C.height() ;
	NcolC = matrice_C.width() ;
	
	printf("\n\n\n") ;
	
	// Calcul de C = A * B au sens des matrices 
	MatMult((double *)matrice_A.begin(), (double *)matrice_B.begin(), (double *)matrice_C.begin(), NlinC, NcolA, NcolC) ;
	
	// Affichage de la matrice C
	it = matrice_C.begin() ;
	for( lin=0 ; lin<NlinC ; lin++)
	{
		for( col=0 ; col<NcolC ; col++, it++)
		{
			printf("[%g]",(*it)) ;
		}
		printf("\n") ;
	}

	/*
	*
	*
	*
	*
	*
	*
	*
	*
	*
	*/

	bool normalize = false;
	std::cout << "Normalize (0) ?" << std::endl;
	int _op = 1;
	std::cin >> _op;
	normalize = (_op == 0);

	CImg<double> TG; 
	CImg<double> TD; 
	if(normalize){
		CImg<double> TG_matrice(3, 3, 1, 1, 0); 
		CImg<double> TD_matrice(3, 3, 1, 1, 0); 
		double moy_xg, moy_xd, moy_yg, moy_yd;
		moy_xg = moy_xd = moy_yg = moy_yd = 0.0;
		for(int i : xd){
			moy_xd += i;
		}
		for(int i : xg){
			moy_xg += i;
		}
		for(int i : yd){
			moy_yd += i;
		}
		for(int i : yg){
			moy_yg += i;
		}
		moy_xd /= nombre_de_points;
		moy_xg /= nombre_de_points;
		moy_yd /= nombre_de_points;
		moy_yg /= nombre_de_points;

		double var_xg, var_xd, var_yg, var_yd;
		var_xg = var_xd = var_yg = var_yd = 0.0;
		for(int i : xd){
			var_xd += std::pow(i-moy_xd,2);
		}
		for(int i : xg){
			var_xg += std::pow(i-moy_xg,2);
		}
		for(int i : yd){
			var_yd += std::pow(i-moy_yd,2);
		}
		for(int i : yg){
			var_yg += std::pow(i-moy_yg,2);
		}
		var_xd = std::sqrt((1/nombre_de_points) * (var_xd));
		var_xg = std::sqrt((1/nombre_de_points) * (var_xg));
		var_yd = std::sqrt((1/nombre_de_points) * (var_yd));
		var_yg = std::sqrt((1/nombre_de_points) * (var_yg));

		TG_matrice(0,0) = var_xg/2.;
		TG_matrice(1,1) = var_yg/2.;
		TG_matrice(2,2) = 1.;
		TG_matrice(0,2) = moy_xg;
		TG_matrice(1,2) = moy_yd;

		TD_matrice(0,0) = var_xd/2.;
		TD_matrice(1,1) = var_yd/2.;
		TD_matrice(2,2) = 1.;
		TD_matrice(0,2) = moy_xd;
		TD_matrice(1,2) = moy_yd;

		TG = TG_matrice.pseudoinvert();
		TD = TD_matrice.pseudoinvert();

		for(unsigned int i = 0 ; i < nombre_de_points ; ++i){
			CImg<double> MMD(3,1,1,1,0);
			CImg<double> MMG(3,1,1,1,0);
			MMD(0,0) = xd[i];
			MMD(0,1) = yd[i];
			MMD(0,2) = 1.;
			MMG(0,0) = xg[i];
			MMG(0,1) = yg[i];  
			MMG(0,2) = 1.;  

			MatMult((double *)TD.begin(), (double *)MMD.begin(), (double *)MMD.begin(), 3, 1, 1) ;
			MatMult((double *)TG.begin(), (double *)MMG.begin(), (double *)MMG.begin(), 3, 1, 1) ;

			xd[i] = MMD(0,0);
			xg[i] = MMG(0,0);
			yd[i] = MMD(1,0);
			yg[i] = MMG(1,0);
		}
	}

    std::vector<unsigned int> alea_indices;
    std::vector<unsigned int> alea_indices_pool = {0,1,2,3,4,5,6,7,8,9,10,11};
    std::srand(static_cast<unsigned int>(std::time(nullptr)));
    for(int i = 0; i<8 ; i++){
        int numeroAleatorio = std::rand() % (alea_indices_pool.size() - 0 + 1) + 0;
        alea_indices.push_back(alea_indices_pool[numeroAleatorio]);
        alea_indices_pool.erase(alea_indices_pool.begin() + numeroAleatorio);
    }
    alea_indices_pool.clear();

    int my_xd[8], my_yd[8], my_xg[8], my_yg[8];
    for( unsigned int i = 0 ; i < 8 ; i++ ){
        my_xd[i] = xd[i];
        my_yd[i] = yd[i];
        my_xg[i] = xg[i];
        my_yg[i] = yg[i];
    }

    std::cout << std::endl << "Working Moindre Carre in progress...." << std::endl;

    CImg<double> fA_matrice(8, 8, 1, 1, 0); 

    for (int i = 0; i < 8; i++) {
        fA_matrice(i, 0) = my_xg[i] * my_xd[i];
        fA_matrice(i, 1) = my_xg[i] * my_yd[i];
        fA_matrice(i, 2) = my_xg[i] ;
        fA_matrice(i, 3) = my_yg[i] * my_xd[i];
        fA_matrice(i, 4) = my_yg[i] * my_yd[i];
        fA_matrice(i, 5) = my_yg[i];
        fA_matrice(i, 6) = my_xd[i];
        fA_matrice(i, 7) = my_yd[i];
    } 

    CImg<double> fB_matrice(1,8,1,1,-1);

    //show_matrice(fA_matrice);
    //show_matrice(fB_matrice,8,1);

    CImg <double> fC1_matrice(8,8,1,1,0) ;
    CImg <double> fC2_matrice(8,1,1,1,0) ;
    CImg <double> f_matrice(8,1,1,1,0) ;

	f_matrice = pseudoinvert(fA_matrice) * (fB_matrice);
	PRINT_MATRIX(f_matrice,8,1);

    std::cout<<std::endl<<"---------------------"<<std::endl;

    std::cout << "disp moindre carre (0)? ";
    int mc = 1;
    std::cin>>mc;
    CImg <double> Mg(3,1,1,1,0), Md(3,1,1,1,0);
    if(mc == 0){
        for(unsigned int i = 0; i<8 ; i++){
            
            Mg(0,0)=my_xg[i]; Md(0,0)=my_xd[i];
            Mg(1,0)=my_yg[i]; Md(1,0)=my_yd[i];
            Mg(2,0)=1.; Md(2,0)=1.;

            CImg <double> Lg(1,3,1,1,0), Ld(1,3,1,1,0);
            std::cout << "-1-" << std::endl; 
            MatMult((double *)f_matrice.transpose().begin(), (double *)Mg.begin(), (double *)Lg.begin(), 8, 3, 1) ;
            MatMult((double *)f_matrice.transpose().begin(), (double *)Md.begin(), (double *)Ld.begin(), 8, 3, 1) ;
			PRINT_MATRIX(Lg, 1, 3);
			std::cout << std::endl;
			PRINT_MATRIX(Lg, 1, 3);


            std::cout << "-2-" << std::endl; 
            int x_inter_lg[2], y_inter_lg[2], x_inter_ld[2], y_inter_ld[2];
            double lg[3], ld[3];
            lg[0] = Lg(0,0); ld[0] = Ld(0,0);
            lg[1] = Lg(0,1); ld[1] = Ld(0,1);
            lg[2] = Lg(0,2); ld[2] = Ld(0,2);
            int np;

            std::cout << "-3-" << std::endl; 
            np = Intersection(Lg, imageG.width(), imageG.height(), x_inter_lg, y_inter_lg ) ;	
            if(np) imageG.draw_line(x_inter_lg[0],y_inter_lg[0],x_inter_lg[1],y_inter_lg[1],argent).display(Gauche_disp);
            np = Intersection(Ld, imageD.width(), imageD.height(), x_inter_ld, y_inter_ld ) ;	
            if(np) imageD.draw_line(x_inter_ld[0],y_inter_ld[0],x_inter_ld[1],y_inter_ld[1],rose).display(Droite_disp);

            std::cout<<std::endl<<"----------"<<i+1<<"-----------"<<std::endl;
        }
    }

	

    std::cout << std::endl << "Working SVD in progress...." << std::endl;

    CImg<double> D(8,8,1,1,0);

	CImg<double> fA2_matrice(8, 9, 1, 1, 0); 

    for (int i = 0; i < 8; i++) {
        fA2_matrice(i, 0) = my_xg[i] * my_xd[i];
        fA2_matrice(i, 1) = my_xg[i] * my_yd[i];
        fA2_matrice(i, 2) = my_xg[i] ;
        fA2_matrice(i, 3) = my_yg[i] * my_xd[i];
        fA2_matrice(i, 4) = my_yg[i] * my_yd[i];
        fA2_matrice(i, 5) = my_yg[i];
        fA2_matrice(i, 6) = my_xd[i];
        fA2_matrice(i, 7) = my_yd[i];
		fA2_matrice(i, 8) = 1;
    } 

    MatMult((double *)fA2_matrice.transpose().begin(), (double *)fA2_matrice.begin(), (double *)D.begin(), 8, 8, 8) ;

    CImgList<float> USV = D.get_SVD();
    CImg<float> V = USV[2];
    V = V.transpose();

    std::cout << "disp SVD (0)? ";
    mc = 1;
    std::cin>>mc;
    f_matrice(0,0) = V(0,0);
    f_matrice(1,0) = V(1,1);
    f_matrice(2,0) = V(2,0);
    f_matrice(3,0) = V(3,0);
    f_matrice(4,0) = V(4,0);
    f_matrice(5,0) = V(5,0);
    f_matrice(6,0) = V(6,0);
    f_matrice(7,0) = V(7,0);
    if(mc == 0){
        for( unsigned int i = 0; i<8 ; i++){
            
            Mg(0,0)=my_xg[i]; Md(0,0)=my_xd[i];
            Mg(1,0)=my_yg[i]; Md(0,0)=my_yd[i];
            Mg(2,0)=1; Md(2,0)=1;

            CImg <double> Lg(1,3,1,1,0), Ld(1,3,1,1,0);
            std::cout << "-1-" << std::endl; 
            MatMult((double *)f_matrice.transpose().begin(), (double *)Mg.begin(), (double *)Lg.begin(), 8, 3, 1) ;
            MatMult((double *)f_matrice.transpose().begin(), (double *)Md.begin(), (double *)Ld.begin(), 8, 3, 1) ;


            std::cout << "-2-" << std::endl; 
            int x_inter_lg[2], y_inter_lg[2], x_inter_ld[2], y_inter_ld[2];
            double lg[3], ld[3];
            lg[0] = Lg(0,0); ld[0] = Ld(0,0);
            lg[1] = Lg(0,1); ld[1] = Ld(0,1);
            lg[2] = Lg(0,2); ld[2] = Ld(0,2);
            int np;

            std::cout << "-3-" << std::endl; 
            np = Intersection(Lg, imageG.width(), imageG.height(), x_inter_lg, y_inter_lg ) ;	
            if(np) imageG.draw_line(x_inter_lg[0],y_inter_lg[0],x_inter_lg[1],y_inter_lg[1],argent).display(Gauche_disp);
            np = Intersection(Ld, imageD.width(), imageD.height(), x_inter_ld, y_inter_ld ) ;	
            if(np) imageD.draw_line(x_inter_ld[0],y_inter_ld[0],x_inter_ld[1],y_inter_ld[1],rose).display(Droite_disp);

            std::cout<<std::endl<<"----------"<<i+1<<"-----------"<<std::endl;
        }
    }

	std::cout << "disp Reduction rang de F (0)? ";
    mc = 1;
    std::cin>>mc;
	if(mc == 0){
		CImgList<float> FUSV = f_matrice.get_SVD();
		FUSV[1](3,3) = 0;
		f_matrice = f_matrice.SVD(FUSV[0],FUSV[1],FUSV[2]);

		if(normalize){
			CImg <double> Ll(3,3,1,1,0);
			MatMult((double *)f_matrice.begin(), (double *)TD.begin(), (double *)Ll.begin(), 3, 3, 1) ;
			MatMult((double *)TG.transpose().begin(), (double *)Ll.begin(), (double *)f_matrice.begin(), 3, 3, 1) ;
		}

		for( unsigned int i = 0; i<8 ; i++){
            
            Mg(0,0)=my_xg[i]; Md(0,0)=my_xd[i];
            Mg(1,0)=my_yg[i]; Md(0,0)=my_yd[i];
            Mg(2,0)=1; Md(2,0)=1;

            CImg <double> Lg(1,3,1,1,0), Ld(1,3,1,1,0);
            std::cout << "-1-" << std::endl; 
            MatMult((double *)f_matrice.transpose().begin(), (double *)Mg.begin(), (double *)Lg.begin(), 8, 3, 1) ;
            MatMult((double *)f_matrice.transpose().begin(), (double *)Md.begin(), (double *)Ld.begin(), 8, 3, 1) ;


            std::cout << "-2-" << std::endl; 
            int x_inter_lg[2], y_inter_lg[2], x_inter_ld[2], y_inter_ld[2];
            double lg[3], ld[3];
            lg[0] = Lg(0,0); ld[0] = Ld(0,0);
            lg[1] = Lg(0,1); ld[1] = Ld(0,1);
            lg[2] = Lg(0,2); ld[2] = Ld(0,2);
            int np;

            std::cout << "-3-" << std::endl; 
            np = Intersection(Lg, imageG.width(), imageG.height(), x_inter_lg, y_inter_lg ) ;	
            if(np) imageG.draw_line(x_inter_lg[0],y_inter_lg[0],x_inter_lg[1],y_inter_lg[1],argent).display(Gauche_disp);
            np = Intersection(Ld, imageD.width(), imageD.height(), x_inter_ld, y_inter_ld ) ;	
            if(np) imageD.draw_line(x_inter_ld[0],y_inter_ld[0],x_inter_ld[1],y_inter_ld[1],rose).display(Droite_disp);

            std::cout<<std::endl<<"----------"<<i+1<<"-----------"<<std::endl;
        }
	}
	
	// Attente de la fermeture d'une des images pour arrÃªter le programme
	while (!Droite_disp.is_closed() && !Gauche_disp.is_closed()) ;
	
	return 0;
}



